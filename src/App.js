// import logo from './logo.svg';
// import './App.css';
import React from 'react';
import Header from './Header/Header';
import Menu from './Menu Tab/Menu';
// import Appnavbar from './Appnavbar'
import "./App.css";

function App() {
  return (
    <div>
      <Header />
      <Menu />
    </div>
  );
}

export default App;
